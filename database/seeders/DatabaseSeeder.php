<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(MethodSeeder::class);
        $this->call(MethodDetailStatusSeeder::class);
        $this->call(MethodDetailSeeder::class);
    }
}
