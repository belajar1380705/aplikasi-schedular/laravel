<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\MethodDetail;

class MethodDetailStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('method_detail_status')->insert([
            [
                'name' => 'Berlangsung',
                'color' => 'badge-primary',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Selesai',
                'color' => 'badge-success',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Akan Datang',
                'color' => 'badge-danger',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]
        ]);
    }
}
