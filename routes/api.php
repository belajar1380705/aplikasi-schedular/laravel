<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\MethodController;
use App\Http\Controllers\Api\MethodDetailController;
use App\Http\Controllers\Api\MethodDetailStatusController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/method', [MethodController::class, 'index']);
Route::post('/method/create', [MethodController::class, 'create']);
Route::put('/method/update/{id}', [MethodController::class, 'update']);
Route::delete('/method/delete/{id}', [MethodController::class, 'delete']);

Route::post('/method-detail/create', [MethodDetailController::class, 'create']);
Route::put('/method-detail/update/{id}', [MethodDetailController::class, 'update']);
Route::delete('/method-detail/delete/{id}', [MethodDetailController::class, 'delete']);

Route::get('/method-detail-status', [MethodDetailStatusController::class, 'index']);