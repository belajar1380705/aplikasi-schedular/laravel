MethodHelper = function() {
    const url = window.location.origin;

    const getData = (param = null) => {
        // * Hapus data di table sebelum get api
        // * fungsi nya untuk refresh data di table / select.
        $('#table-main tr:gt(0)').remove();
        $('.method-selection option').remove();

        // * Check apakah parameter di function ini ada atau tidak.
        let getUrl = `${url}/api/method`;
        
        
        if (param != null && param != '') {
            // * Ganti Url Menggunakan Param Tahun.
            getUrl = `${url}/api/method?year=${param}`;

            // * Untuk menambahkan tahun di table misal januari "2022"
            $('.PutYear').html(param);
        } else {
            $('#SelectYear').val('');
            $('.PutYear').html('');
        }

        // * Get data dari API
        $.ajax({
            url: getUrl,
            type: 'GET',
            dataType: 'json',
            success: function(response) {
                $.each(response['data'], function(index, data) {
                    var name = data.name;
                    
                    // * Generate Data Per Bulan.
                    const januari = generateData(data['januari']);
                    const februari = generateData(data['februari']);
                    const maret = generateData(data['maret']);
                    const april = generateData(data['april']);
                    const mei = generateData(data['mei']);
                    const juni = generateData(data['juni']);
                    const july = generateData(data['july']);
                    const agustus = generateData(data['agustus']);
                    const september = generateData(data['september']);
                    const oktober = generateData(data['oktober']);
                    const november = generateData(data['november']);
                    const desember = generateData(data['desember']);

                    // * Append To Table
                    $('#table-main').append(`
                    <tr>
                    <td class="GetMethodData" data-id="${data.id}" data-name="${data.name}"><a href="#ModalUpdateMethod" data-toggle="modal" data-title="Judul Modal">${name}</a></td>
                    <td>${januari}</td>
                    <td>${februari}</td>
                    <td>${maret}</td>
                    <td>${april}</td>
                    <td>${mei}</td>
                    <td>${juni}</td>
                    <td>${july}</td>
                    <td>${agustus}</td>
                    <td>${september}</td>
                    <td>${oktober}</td>
                    <td>${november}</td>
                    <td>${desember}</td>
                    </tr>
                    `);

                    // * Append To Select Option
                    $('.method-selection').append(`
                        <option value="${data.id}">${data.name}</option>
                    `);
                });
            },
            error: function(xhr, status, error) {
                if (xhr.responseJSON.meta.status == 'error') {
                    toastr.warning(xhr.responseJSON.meta.message);
                }
            }
        });
    }

    const generateData = (responses) => {
        let string = '';

        $.each(responses, function(index, response) {
            string += `
            <ul>
                <li class="GetMethodDetailData" data-id="${response.id}" data-name="${response.name}" data-method="${response.method_id}" data-start="${response.start_date}" data-end="${response.end_date}" data-status="${response.status}"><a href="#ModalUpdateMethodDetail" class="text-decoration-none text-reset" data-toggle="modal" data-title="Judul Modal">${response.name} <br> <span class="text-primary">${response.date}</span> <br> <span class="badge ${response.status_color}">${response.status_text}</span></a></li>
            </ul>`;
        })  
        
        return string;
    }


    const fillter = () => {
        $('#SelectYear').change(function() {
            var selectedValue = $(this).val();
            
            // * Passing paramter ke function get data supaya di render ulang.
            getData(selectedValue);
        }); 
    }
    
    const fillSelectYear = () => {
        // * Fungsi ini untuk menambahkan Option tahun untuk fillter.
        let currentYear = new Date().getFullYear();
        for (let index = 2015; index <= currentYear; index++) {
            $('#SelectYear').append(`<option value="${index}">${index}</option>`);
        }
    }

    const selectStatus = () => {
        // * Fungsi ini untuk menambahkan Data Option status (Dynamis)
        $.ajax({
            url: `${url}/api/method-detail-status`,
            type: 'GET',
            dataType: 'json',
            success: function(response) {
                $.each(response['data'], function(index, data) {
                    // * Append To Select Field
                    $('.SelectStatus').append(`<option value="${data.id}">${data.name}</option>`);
                });
            },
            error: function(xhr, status, error) {
                if (xhr.responseJSON.meta.status == 'error') {
                    toastr.warning(xhr.responseJSON.meta.message);
                }
            }
        });
    }

    const createData = () => {
        $('#FormCreateMethod').on('submit', function(e) {
            e.preventDefault(); // mencegah submit form secara default
            $('#ConfirmCreateButton').prop('disabled', true);
            $('#ConfirmCreateButton').html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span><span class="sr-only">Loading...</span> Sedang Diproses ...');
            
            $.ajax({
                url: `${url}/api/method/create`, // alamat tujuan
                type: 'POST', // tipe request
                data: $(this).serialize(), // data yang akan dikirim, di-serialize dari form
                success: function(response) {
                    // * Disable The Button
                    $('#ModalCreateMethod').modal('hide');             
                    
                    // * Remove previous value from input
                    $('#NameCreateMethod').val('');       
                    
                    setTimeout(function() {
                        $('#ConfirmCreateButton').prop('disabled', false);
                        $('#ConfirmCreateButton').html('Konfirmasi');
                    }, 1000);

                        toastr.success('Metode berhasil dibuat.');
                    
                    // * Refresh Data
                    getData();
                },
                error: function(xhr, status, error) {
                    $('#ConfirmCreateButton').prop('disabled', false);
                    $('#ConfirmCreateButton').html('Konfirmasi');
                    if (xhr.responseJSON.meta.status == 'error') {
                        toastr.warning(xhr.responseJSON.meta.message);
                    }
                }
            });
        });
    };

    const updateData = () => {
        $(document).on('click', '.GetMethodData', function() {
            $('#IDUpdateMethod').val($(this).data('id'));
            $('#NameUpdateMethod').val($(this).data('name'));
        });

        $('#FormUpdateMethod').on('submit', function(e) {
            e.preventDefault(); // mencegah submit form secara default
            $('#ConfirmUpdateButton').prop('disabled', true);
            $('#ConfirmUpdateButton').html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span><span class="sr-only">Loading...</span> Sedang Diproses ...');
            
            const getMethodID = $('#IDUpdateMethod').val();

            $.ajax({
                url: `${url}/api/method/update/${getMethodID}`, // alamat tujuan
                type: 'PUT', // tipe request
                data: $(this).serialize(), // data yang akan dikirim, di-serialize dari form
                success: function(response) {
                    // * Disable The Button
                    $('#ModalUpdateMethod').modal('hide');                    
                    
                    setTimeout(function() {
                        $('#ConfirmUpdateButton').prop('disabled', false);
                        $('#ConfirmUpdateButton').html('Konfirmasi');
                    }, 1000);

                    toastr.success('Metode berhasil Diedit.');
                    
                    // * Refresh Data
                    getData();

                },
                error: function(xhr, status, error) {
                    // jika request gagal tampilkan error
                    $('#ConfirmUpdateButton').prop('disabled', false);
                    $('#ConfirmUpdateButton').html('Konfirmasi');
                    if (xhr.responseJSON.meta.status == 'error') {
                        toastr.warning(xhr.responseJSON.meta.message);
                    }
                }
            });
        });
    };

    const deleteData = () => {
        $(document).on('click', '.GetMethodData', function() {
            $('#IDDeleteMethod').val($(this).data('id'));
        });

        $('#FormDeleteMethod').on('submit', function(e) {
            e.preventDefault();

            $('#ConfirmDeleteButton').prop('disabled', true);
            $('#ConfirmDeleteButton').html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span><span class="sr-only">Loading...</span> Sedang Diproses ...');

            const getMethodID = $('#IDDeleteMethod').val();

            console.log(getMethodID);
            $.ajax({
                url: `${url}/api/method/delete/${getMethodID}`, // alamat tujuan
                type: 'DELETE', // tipe request
                data: $(this).serialize(), // data yang akan dikirim, di-serialize dari form
                success: function(response) {
                    console.log('smape ?');
                    // * Disable The Button
                    $('#ModalUpdateMethod').modal('hide');                    
                    
                    setTimeout(function() {
                        $('#ConfirmDeleteButton').prop('disabled', false);
                        $('#ConfirmDeleteButton').html('Ya, Hapus Data!');
                    }, 1000);

                    toastr.success('Metode berhasil Dihapus.');
                    
                    // * Refresh Data
                    getData();

                },
                error: function(xhr, status, error) {
                    $('#ConfirmDeleteButton').prop('disabled', false);
                    $('#ConfirmDeleteButton').html('Konfirmasi');
                    if (xhr.responseJSON.meta.status == 'error') {
                        toastr.warning(xhr.responseJSON.meta.message);
                    }
                }
            });
        });        
    }

    const createDetailData = () => {
        $('#FormCreateMethodDetail').on('submit', function(e) {
            e.preventDefault(); // mencegah submit form secara default
            // console.log('masuk sini');
            $('#ConfirmCreateButtonMethodDetail').prop('disabled', true);
            $('#ConfirmCreateButtonMethodDetail').html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span><span class="sr-only">Loading...</span> Sedang Diproses ...');
            
            $.ajax({
                url: `${url}/api/method-detail/create`, // alamat tujuan
                type: 'POST', // tipe request
                data: $(this).serialize(), // data yang akan dikirim, di-serialize dari form
                success: function(response) {
                    console.log(response);
                    // * Disable The Button
                    $('#ModalCreateMethodDetail').modal('hide');    
                    
                    // Reset Value from previous input
                    $('#MethodCreateMethodDetail').val('');
                    $('#NameCreateMethodDetail').val('');
                    $('#StartCreateMethodDetail').val('2019-01-01');
                    $('#EndCreateMethodDetail').val('2019-01-01');
                    
                    setTimeout(function() {
                        $('#ConfirmCreateButtonMethodDetail').prop('disabled', false);
                        $('#ConfirmCreateButtonMethodDetail').html('Konfirmasi');
                    }, 1000);

                    toastr.success('Metode berhasil dibuat.');
                    
                    // * Refresh Data
                    getData();
                },
                error: function(xhr, status, error) {
                    // jika request gagal
                    $('#ConfirmCreateButtonMethodDetail').prop('disabled', false);
                    $('#ConfirmCreateButtonMethodDetail').html('Konfirmasi');
                    if (xhr.responseJSON.meta.status == 'error') {
                        toastr.warning(xhr.responseJSON.meta.message);
                    }
                }
            });
        });
    };

    const updateDetailData = () => {
        $(document).on('click', '.GetMethodDetailData', function() {
            console.log($(this).data('method'));
            $('#IDUpdateMethodDetail').val($(this).data('id'));
            $('#MethodUpdateMethodDetail').val($(this).data('method'));
            $('#NameUpdateMethodDetail').val($(this).data('name'));
            $('#StartUpdateMethodDetail').val($(this).data('start'));
            $('#EndUpdateMethodDetail').val($(this).data('end'));
            $('#StatusUpdateMethodDetail').val($(this).data('status'));
        });

        $('#FormUpdateMethodDetail').on('submit', function(e) {
            e.preventDefault(); // mencegah submit form secara default
            $('#ConfirmUpdateButtonMethodDetail').prop('disabled', true);
            $('#ConfirmUpdateButtonMethodDetail').html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span><span class="sr-only">Loading...</span> Sedang Diproses ...');
            
            const getMethodID = $('#IDUpdateMethodDetail').val();

            $.ajax({
                url: `${url}/api/method-detail/update/${getMethodID}`, // alamat tujuan
                type: 'PUT', // tipe request
                data: $(this).serialize(), // data yang akan dikirim, di-serialize dari form
                success: function(response) {
                    console.log(response);
                    // * Disable The Button
                    $('#ModalUpdateMethodDetail').modal('hide');                    
                    
                    setTimeout(function() {
                        $('#ConfirmUpdateButtonMethodDetail').prop('disabled', false);
                        $('#ConfirmUpdateButtonMethodDetail').html('Konfirmasi');
                    }, 1000);

                    toastr.success('Kegiatan berhasil Diedit.');
                    
                    // * Refresh Data
                    getData();

                },
                error: function(xhr, status, error) {
                    // jika request gagal
                    $('#ConfirmUpdateButtonMethodDetail').prop('disabled', false);
                    $('#ConfirmUpdateButtonMethodDetail').html('Konfirmasi');
                    if (xhr.responseJSON.meta.status == 'error') {
                        toastr.warning(xhr.responseJSON.meta.message);
                    }
                }
            });
        });
    };

    const deleteDetailData = () => {
        $(document).on('click', '.GetMethodDetailData', function() {
            $('#IDDeleteMethodDetail').val($(this).data('id'));
        });

        $('#FormDeleteMethodDetail').on('submit', function(e) {
            e.preventDefault();

            $('#ConfirmDeleteButtonMethodDetail').prop('disabled', true);
            $('#ConfirmDeleteButtonMethodDetail').html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span><span class="sr-only">Loading...</span> Sedang Diproses ...');

            const getMethodID = $('#IDDeleteMethodDetail').val();

            $.ajax({
                url: `${url}/api/method-detail/delete/${getMethodID}`, // alamat tujuan
                type: 'DELETE', // tipe request
                data: $(this).serialize(), // data yang akan dikirim, di-serialize dari form
                success: function(response) {
                    console.log(response);
                    // * Disable The Button
                    $('#ModalUpdateMethodDetail').modal('hide');                    
                    
                    setTimeout(function() {
                        $('#ConfirmDeleteButtonMethodDetail').prop('disabled', false);
                        $('#ConfirmDeleteButtonMethodDetail').html('Ya, Hapus Data!');
                    }, 1000);

                    toastr.success('Kegiatan berhasil Dihapus.');
                    
                    // * Refresh Data
                    getData();

                },
                error: function(xhr, status, error) {
                    // jika request gagal
                    $('#ConfirmDeleteButtonMethodDetail').prop('disabled', false);
                    $('#ConfirmDeleteButtonMethodDetail').html('Konfirmasi');
                    if (xhr.responseJSON.meta.status == 'error') {
                        toastr.warning(xhr.responseJSON.meta.message);
                    }
                }
            });
        });        
    }

    return {
        init: function() {
            // * Use function above here.
            getData();
            createData();
            updateData();
            deleteData();
            createDetailData();
            updateDetailData();
            deleteDetailData();
            fillter();
            fillSelectYear();
            selectStatus();
        }
    };

}();

jQuery(document).ready(function() {
    MethodHelper.init();
});
