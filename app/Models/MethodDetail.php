<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Method;
use App\Models\MethodDetailStatus;

class MethodDetail extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'method_detail';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'method_id', 'name', 'status', 'start_date', 'end_date',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at',
    ];

    /**
     * Get the method that owns the method detail.
     */
    public function method()
    {
        return $this->belongsTo(Method::class);
    }

        /**
     * Get the method that owns the method detail.
     */
    public function methodDetailStatus()
    {
        return $this->belongsTo(methodDetailStatus::class, 'status', 'id');
    }
}