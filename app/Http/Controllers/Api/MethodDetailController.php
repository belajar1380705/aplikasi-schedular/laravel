<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Method;
use App\Models\MethodDetail;
use App\Http\Requests\MethodDetailCreateRequest;
use App\Http\Requests\MethodDetailUpdateRequest;

class MethodDetailController extends Controller
{
    // * Please open this one "App\Http\Controllers\Controller"
    // * and See the traits Helper

    public function create(MethodDetailCreateRequest $request) 
    {
        try {
            $response = \DB::transaction(function() use ($request) {
                if ($request->end_date < $request->start_date) {
                    return $this->errorResponse('Tanggal Selesai tidak boleh kurang dari tanggal mulai.', 422);
                }
        
                // * check the method
                $method = Method::where('id', $request->method_id)->first();
                if (!$method) {
                    return $this->errorResponse('Metode tidak ditemukan.', 422);
                }
        
                // * Create the data.
                $response = MethodDetail::create([
                    'name' => $request->name,
                    'method_id' => $request->method_id,
                    'start_date' => $request->start_date,
                    'end_date' => $request->end_date,
                    'status' => $request->status
                ]);
        
                return $this->successResponse('Berhasil membuat data', $response, 200);
            });

            return $response;
        } catch (\Throwable $th) {
            return $this->errorResponse('Internal Server Error', 500);
        }
    }

    public function update(MethodDetailUpdateRequest $request, $id)
    {   
        try {
            $response = \DB::transaction(function() use ($request, $id) {
                $method = Method::where('id', $request->method_id)->first();
                if (!$method) {
                    return $this->errorResponse('Metode tidak ditemukan.', 422);
                }
        
                $checkData = MethodDetail::where('id', $id)->first();
                if (!$checkData) {
                    return $this->errorResponse('Kegiatan tidak ditemukan', 422);
                }
        
                $response = MethodDetail::where('id', $id)->update([
                    'name' => $request->name,
                    'method_id' => $request->method_id,
                    'start_date' => $request->start_date,
                    'end_date' => $request->end_date,
                    'status' => $request->status
                ]);
        
                return $this->successResponse('Berhasil mengedit data', [], 200);
            });

            return $response;
        } catch (\Throwable $th) {
            return $this->errorResponse('Internal Server Error', 500);
        }
    }

    public function delete($id)
    {
        try {
            $response = \DB::transaction(function() use ($id) {
                $checkData = MethodDetail::where('id', $id)->first();
                if (!$checkData) {
                    return $this->errorResponse('Kegiatan tidak ditemukan.', 422);
                }
        
                MethodDetail::where('id', $id)->delete();
                
                return $this->successResponse('Berhasil Menghapus data', [], 200);
            });

            return $response;
        } catch (\Throwable $th) {
            return $this->errorResponse('Internal Server Error', 500);
        }
    }
}
