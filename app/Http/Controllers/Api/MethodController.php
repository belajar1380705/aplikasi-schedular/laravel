<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\MethodCreateRequest;
use App\Http\Requests\MethodUpdateRequest;
use Illuminate\Http\Request;
use App\Models\Method;
use App\Models\MethodDetail;
use App\Http\Resources\GetMethodResource;

class MethodController extends Controller
{

    // * Please open this one "App\Http\Controllers\Controller"
    // * and See the traits Helper

    public function index(Request $request) 
    {
        try {
            // * Get Data method with method_detail relation.
            $methods = Method::with(['methodDetail' => function($query) use ($request) {
                if ($request->has('year')) {
                    $query->whereYear('start_date', $request->input('year'));
                }
                $query->orderBy('id', 'ASC');
            }])->orderBy('id', 'ASC')->get();

            // * Process Data
            $response = $this->processData($methods);
    
            // * Return the response.
            return $this->successResponse('Berhasil Mendapatkan Data', GetMethodResource::collection($response), 200);
        } catch (\Throwable $th) {
            return $this->errorResponse('Internal Server Error', 500);
        }
    }

    public function processData($methods) 
    {
        // * Define Base Month
        $month = '';
        
        // * Generate Array Months
        $arrayMonths = ['januari', 'februari', 'maret', 'april', 'mei', 'juni', 'july', 'agustus', 'september', 'oktober', 'november', 'desember'];

        // * Generate new Resources.
        $newCollection = [];
        foreach ($methods as $key => $method) {

            // * Generate New Collection
            $newCollection[$key]['id'] = $method->id;
            $newCollection[$key]['name'] = $method->name;
            for ($i=0; $i < count($arrayMonths); $i++) { 
                $newCollection[$key][$arrayMonths[$i]] = [];
            }

            foreach ($method->methodDetail as $methodDetail) {
                $expStartDate = explode('-', $methodDetail->start_date);
                $month = $this->getMonth($expStartDate[1]);
                array_push($newCollection[$key][$month], [
                    'id' => $methodDetail->id,
                    'name' => $methodDetail->name,
                    'status' => $methodDetail->status,
                    'method_id' => $methodDetail->method_id,
                    'start_date' => $methodDetail->start_date,
                    'end_date' => $methodDetail->end_date,
                    'status_text' => $methodDetail->methodDetailStatus->name,
                    'status_color' => $methodDetail->methodDetailStatus->color,
                    'date' => '(' . date('d/m/Y', strtotime($methodDetail->start_date)) . ' - ' . date('d/m/Y', strtotime($methodDetail->end_date)) . ')'
                ]);
            }
        }

        return $newCollection;
    }

    public function create(MethodCreateRequest $request) 
    {
        try {
            $response =\DB::transaction(function() use ($request) {
                // * get Method
                $method = Method::where('name', $request->name)->first();
        
                // * check is the name is exists
                if ($method) {
                    return $this->errorResponse('Nama Sudah digunakan', 422);
                }
        
                $response = Method::create([
                    'name' => $request->name
                ]);
        
                return $this->successResponse('Berhasil Membuat Data', $response, 200);
            });

            return $response;
        } catch (\Throwable $th) {
            return $this->errorResponse('Internal Server Error', 500);
        }

    }

    public function update(MethodUpdateRequest $request, $id)
    {
        try {
            $response = \DB::transaction(function() use ($request, $id) {
                $checkData = Method::where('id', $id)->first();
                if (!$checkData) {
                    return $this->errorResponse('Metode tidak ditemukan!', 422);
                }
        
                // * check is the name is exists
                $method = Method::where('id', '!=', $id)->where('name', $request->name)->first();
        
                // * create data
                if ($method) {
                    return $this->errorResponse('Nama Sudah digunakan', 422);
                }
        
                $response = Method::where('id', $id)->update([
                    'name' => $request->name
                ]);
                
                return $this->successResponse('Berhasil Mengedit Data', [], 200);
            });

            return $response;
        } catch (\Throwable $th) {
            return $this->errorResponse('Internal Server Error', 500);
        }
    }

    public function delete($id)
    {
        try {
            $response = \DB::transaction(function() use ($id) {
                $checkData = Method::where('id', $id)->first();
                if (!$checkData) {
                    return $this->errorResponse('Metode tidak ditemukan', 422);
                }
        
                $methodDetail = MethodDetail::where('method_id', $checkData->id)->get();
                if(count($methodDetail) > 0) {
                    MethodDetail::where('method_id', $checkData->id)->delete();
                }
        
                Method::where('id', $id)->delete();
        
                return $this->successResponse('Berhasil Menghapus Data', [], 200);
            });

            return $response;
        } catch (\Throwable $th) {
            return $this->errorResponse('Internal Server Error', 500);
        }
    }
}
