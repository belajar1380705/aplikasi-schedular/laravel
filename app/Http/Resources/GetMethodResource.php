<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class GetMethodResource extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this['id'],
            'name' => $this['name'],
            'januari' => $this['januari'],
            'februari' => $this['februari'],
            'maret' => $this['maret'],
            'april' => $this['april'],
            'mei' => $this['mei'],
            'juni' => $this['juni'],
            'july' => $this['july'],
            'agustus' => $this['agustus'],
            'september' => $this['september'],
            'oktober' => $this['oktober'],
            'november' => $this['november'],
            'desember' => $this['desember'],
        ];
    }
}
