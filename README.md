## Requirement

- Composer Version 2.5.4
- php versi 8.2.7
- laravel versi 8.83.27
- database: postgresql

## How to Run this project ?

- composer install
- cp .env.example .env
- php artisan key:generate
- change database environment variable value on .env
- php artisan migrate --seed
- php artisan serve

## Tech Stack
- laravel
- Jquery
- Postgresql
