<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title> {{config('app.name')}} </title>
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/toast.min.css') }}">
</head>
<body>

    <nav class="navbar navbar-light bg-light">
    <a class="navbar-brand" href="#">
        Testing App
    </a>
    </nav>

    <div class="container-fluid">
        <div class="row" id="body-row">
            <div class="col p-4">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                @yield('content')
            </div>
        </div>
    </div>

    <script src="{{ asset('assets/js/jquery.js') }}">
    <script src="{{ asset('assets/js/popper.js') }}" charset="utf-8"></script>
    <script src="{{ asset('assets/js/bootstrap.js') }}" charset="utf-8"></script>
    <script src="{{ asset('assets/js/toast.min.js') }}"></script>
    <script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
        }
    });
    </script>
    @stack('scripts')
</body>
</html>
